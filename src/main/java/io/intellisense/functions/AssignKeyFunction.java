package io.intellisense.functions;

import io.intellisense.data.DataPoint;
import io.intellisense.data.KeyedDataPoint;
import org.apache.flink.api.common.functions.MapFunction;

public class AssignKeyFunction implements MapFunction<DataPoint<Double>, KeyedDataPoint<Double>> {

    private String key;

    public AssignKeyFunction(String key) {
        this.key = key;
    }

    @Override
    public KeyedDataPoint<Double> map(DataPoint<Double> dataPoint) {
        return dataPoint.withKey(key);
    }
}
