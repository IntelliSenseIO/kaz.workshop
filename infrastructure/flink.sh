#!/bin/bash

# Start cluster with 1 taskmanager
~bin/start-cluster.sh

# add taskmanager
~bin/taskmanager.sh start

# find taskmanager PID
lsof -i :34093